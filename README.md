# Chatapp
Chat app with usual chat app features
Certainly! Here are the descriptions for the 2nd, 3rd, and 4th sections of the documentation:

## 1. Getting Started

### Installation

To set up the Social Network Application backend, follow these steps:

1. Clone the repository from [GitHub Repository URL].
```
git clone [GitHub Repository URL]
```
2. Navigate to the project directory.
3. Install the dependencies by running the following command on terminal:
   ```
   npm install
   ```

### Configuration

Before running the application, you need to configure the following settings:

1. Database Configuration:
   - Open the `.env` file in the project directory.
   - **Set the `PORT` and `MONGO_URI` variable to your MongoDB connection string.**

### Running the Application

To start the Social Network Application backend, use the following command:

```
npm start
```

The application will start running on `http://localhost:5000`or or with your defined port in the .env “PORT” variable.



## 2. API Endpoints

### Profile Management

- **Create User (`POST /api/users/`)**: This endpoint is to create a User. The request should include the 'name', 'email', 'password'.

- **login User (`POST /api/users/login`)**:This endpoint is to login a User. The request should include the 'email' and 'password'. You will recieve a JWT token you have to save it for authentication and send the JWT everytime you make request other.

- **Update User Profile (`PUT /api/users/:userID`)**: This endpoint allows users to update their profile information. The request should include the `userId` parameter in the URL.

### Chat Messaging

There are 3 collections in this app: User, Chat and message. In chat collection, We are storing data of chats we make. It could either be one on one chat or group chat. In Message collection, we are storing the content of the message and the Chat id to tell from which type of chat it is associated with. More Schema detail is given below. The following APIs were  created keeping in mind the above collection.

**Note:- you need to send the jwt token in bearer param* for using chat APIs you can generate it by login.**

- **Send Message(`POST /api/message/`)**: This endpoint enables users to send message but The request should include the 'chatId' and message content in 'content'.

- **Get all chat message(`GET /api/message/:chatId`)**: This endpoint enables users to get all messages which associated with the chat id. The request should include the `chatId` parameter in the URL.

- **Fetch chats(`GET /api/chat/`)**: This endpoint enables users to fetch all chats for a user. 

- **Create Group(`POST /api/chat/group`)**: This endpoint enables users to Create New Group Chat. this should include users array and 'name'(Group name or Chat name). users arrays are as "users":["id1","id2"]

- **Rename Group(`PUT /api/chat/rename`)**: This endpoint enables users to rename the group. The request should include the 'chatID' and 'chatName'(new name of the group).

- **Add in a Group(`PUT /api/chat/groupadd`)**: This endpoint enables users to Add user to Group. The request should include the 'chatID' and 'userId'.

- **Remove from a Group(`PUT /api/chat/groupremove`)**: This endpoint enables users to Remove user from Group. The request should include the 'chatID' and 'userId'.

- **Create one-on-one chat (`POST /api/chat/`)**: This endpoint enables users to Create or fetch One to One Chat. The request should include the user id.
Note:- you need to send the jwt token which you recieve after login.

- **Receive Message (Real-time)**: The application utilizes real-time communication through WebSocket for instant messaging. Messages are broadcasted to the relevant users in real-time, enabling them to receive messages as they are sent.

## 3. Database Schema

The Social Network Application backend uses MongoDB for data storage. Here are the schema details for the database collections:

### User Schema

- **name** (String, required): The name of the user.
- **email** (String, required, unique): The email address of the user.
- **password** (String, required): The user's password.

### Chat Schema

- **chatName:**(String): The name of the chat
- **users** (ObjectId **array**): The ID of the users associated with the chat(one-on-one or in group ).
- **isGroupChat** (Boolean, default:false): False for One-on-One chat and true for group chat.
- **latestMessage** (ObjectId): The ID of the last chat to show the latest message on front end.
- **groupAdmin** (ObjectId): The ID of the user who is admin if it is a group.
- **timestamp** (Datetime): The timestamp indicating when the message was sent.

### Message Schema

- **sender** (ObjectId): The ID of the user who sent the message.
- **chat** (ObjectId): The ID of the chat which reference for the chat.
- **content** (String): The content of the message.
- **timestamp** (Datetime): The timestamp indicating when the message was sent.

These schemas define the structure of the User, Chat and Message collections in the MongoDB database and provide the necessary fields for storing user and message-related data.

## 4. Tech or Libraries Used
The Social Network Application backend is built using the following technologies and libraries:

- **Node.js**: A JavaScript runtime environment that allows running JavaScript code on the server-side.
- **Express.js**: A web application framework for Node.js, used for handling HTTP requests and routing.
- **MongoDB**: A NoSQL database for storing and retrieving data.
- **Mongoose**: An Object Data Modeling (ODM) library for MongoDB, providing a higher-level abstraction for interacting with the database.
- **Socket.IO**: A library that enables real-time, bidirectional communication between the server and clients through WebSocket.
- **JWT**: JSON Web Tokens are used for authentication and securing API endpoints.
- **Express Async Handler**: A library that simplifies error handling and async/await syntax in Express.js.
- **Express Validator**: A middleware that provides convenient request validation and sanitization.
- **Nodemon**: A tool that automatically restarts the server whenever changes are made in the code, making the development process more efficient.
- **dotenv**: A module for loading environment variables from a `.env` file into the application.
- **color**: A library that adds colors and styles to the console output for enhanced readability.

These technologies, libraries, and tools were chosen to streamline the development process, enhance code quality, and provide necessary functionalities for building the Social Network Application backend.
