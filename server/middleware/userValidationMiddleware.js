const { body } = require("express-validator");

const validateProfileData = [
  body("Username").notEmpty().withMessage("username is required"),
  body("email")
    .notEmpty()
    .withMessage("Email is required")
    .isEmail()
    .withMessage("Email must be valid"),
  //   body("password").notEmpty().withMessage("Password is required"),
];

module.exports = validateProfileData;
