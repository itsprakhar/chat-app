const express = require("express");
const router = express.Router();
const {
  registerUser,
  updateUser,
  authUser,
  allUsers,
} = require("../controllers/userController");
const validateProfileData = require("../middleware/userValidationMiddleware");

router.route("/").post(registerUser);
router.post("/login", authUser);

router.route("/:userId").put(validateProfileData, updateUser);

module.exports = router;
